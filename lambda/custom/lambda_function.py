# -*- coding: utf-8 -*-
from flask import Flask, render_template
from flask_ask import Ask, statement, question, request, session
from slackclient import SlackClient


app = Flask(__name__)
ask = Ask(app, '/')
sc = SlackClient('xoxp-4786776905-478938654067-563291157107-5eacc85fe576b107b43a5eadb14292e0')

# SLACK_CHANNEL = '@sean'
SLACK_CHANNEL = '#machine_learning'
mapping = {}

def resolved_values(r):
    """
    This function is necessary because the slot values passed to the Intent is what is spoken,
    rather than the canonical value. This makes sure canonical value is used,.

    Takes the request JSON and converts it into a dictionary of your intent
    slot names with the resolved value.

    Example usage:

    resolved_vals = resolved_values(request)
    txt = ""
    for key, val in resolved_vals.iteritems():
        txt += "\n{}:{}".format(key, val)


    :param request: request JSON
    :return: {intent_slot_name: resolved_value}
    """
    slots = r["intent"]["slots"]
    slot_names = slots.keys()

    resolved_vals = {}

    for slot_name in slot_names:
        slot = slots[slot_name]

        if "resolutions" in slot:
            slot = slot["resolutions"]["resolutionsPerAuthority"][0]
            slot_status = slot["status"]["code"]
            if slot_status == "ER_SUCCESS_MATCH":
                resolved_val = slot["values"][0]["value"]["name"]
                resolved_vals[slot_name] = resolved_val
            else:
                resolved_vals[slot_name] = None
        else:  # No value found for this slot value
            resolved_vals[slot_name] = None
    return resolved_vals


def calculate_total(responses):
    """ Calculates the total score from the questionnaire """
    total = 0
    errors = 0
    for k in responses:
        try:
            total += int(responses[k])
        except:
            errors += 1
    return total


def do_slack(channel, txt):
    sc.api_call(
        "chat.postMessage",
        channel=channel,
        text=txt
    )


"""
Intents are below here
"""

@ask.launch
def launched():
    intro_text = render_template('intro')
    return question(intro_text)


@ask.intent('QuestionnaireIntent')
def questionnaire():
    values = resolved_values(request)
    session.attributes['results'] = values
    total = calculate_total(values)
    if total >= 9:
        speech_text = render_template('results_high')
    elif total >= 6:
        speech_text = render_template('results_medium')
    else:
        speech_text = render_template('results_low')
    speech_text = render_template('send_results', prelude=speech_text)
    return question(speech_text).simple_card('Results', speech_text)


@ask.intent('SendResultsIntent')
def send_results():
    if 'results' in session.attributes and session.attributes['results']:
        do_slack(SLACK_CHANNEL, render_template('format_results', responses=session.attributes['results']))
        speech_text = 'I have sent the results'
    else:
        speech_text = 'The results are empty'
    return statement(speech_text).simple_card('Sending Results', speech_text)


# In AWS Lambda context
def lambda_handler(event, _context):
    return ask.run_aws_lambda(event)


# main execution
if __name__ == '__main__':
    app.run()


