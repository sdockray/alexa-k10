# K10 Questionnaire - Alexa Skill

## TODO

Document deployment to Lambda via command line

## Installation

1. Make sure you have an [Amazon Developer account](http://developer.amazon.com/)

2. [Install the ASK Cli](https://developer.amazon.com/docs/smapi/ask-cli-intro.html). On install this will prompt you for both your Amazon Developer account and also an AWS account (which comes in handy if you want to deploy to Lambda).

3. Clone this repository using ASK CLI

    `ask new --url https://bitbucket.org/sdockray/alexa-k10.git`

4. Inside the cloned repository, initialize the Skill (after this it will show up in the Amazon Developer list of Skills):

    ```
    ask api create-skill -f skill.json
    
    ask deploy
    ```
    
    Alternatively, you also could 
    just do it through the Skill Builder interface (within the Amazon Developer website). You could copy and 
    paste the JSON from `models/` into the Interaction Model section to define the Invocation, the Intents, 
    and the Slots and Slot Types.

5. Install the Python requirements, within `lambda/custom/` with: 

    `pip install -r requirements.txt -t .`
    
    This will install the libraries directly into the same folder, which is a requirement for AWS Lambda deployment.

6. The endpoints in `.ask/config` and `skill.json` are wrong (they work for my project right now). You will need to change this 
 to reflect either using AWS Lambda or your own development endpoint. For development, I am using __ngrok__ by first starting 
 the Flask web server and then exposing it via ngrok:
 
    ```
    cd lambda/custom
    
    python lambda_function.py
    ```
    
    and then in another terminal window:
    
    ```
    ngrok http 5000
    ```

